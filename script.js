let data;

let leakyReluModel;
let reluModel;
let sigmoidModel;
let tanhModel;

let newRows = 0;
let leakyReluModelPrecision = 0;
let reluModelPrecision = 0;
let sigmoidModelPrecision = 0;
let tanhModelPrecision = 0;

window.addEventListener("load", function() {
  loadData();
  stickyTable();
  setupModel();
});

function setupModel() {
  let modelConfig = {
    binaryThresh: 0.5,
    inputSize: 11,
    outputSize: 10,
    hiddenLayers: [23],
    hiddenLayers: [9, 7, 5, 3],
    leakyReluAlpha: 0.000005,
    learningRate: 0.00005,
  };

  leakyReluModel = new brain.NeuralNetwork({ ...modelConfig, activation: 'leaky-relu' });
  reluModel = new brain.NeuralNetwork({ ...modelConfig, activation: 'relu' });
  sigmoidModel = new brain.NeuralNetwork({ ...modelConfig, activation: 'sigmoid' });
  tanhModel = new brain.NeuralNetwork({ ...modelConfig, activation: 'tanh' });

  trainModels();
}

function stickyTable() {
  if (!document.querySelector('tbody').innerHTML) {
    return;
  }
  
  let table = document.querySelector('table');
  let thead = document.querySelector('thead');
  let tfoot = document.querySelector('tfoot');
  
  table.style.marginTop = `${thead.offsetHeight}px`;
  table.style.marginBottom = `${tfoot.offsetHeight}px`;

  thead.style.position = 'fixed';
  thead.style.top = '0px';
  thead.style.width = `${table.offsetWidth}px`;

  tfoot.style.position = 'fixed';
  tfoot.style.top = `calc(100vh - ${tfoot.offsetHeight}px)`;
  tfoot.style.width = `${table.offsetWidth}px`;

  Object.values(document.querySelectorAll('tbody tr')?.item(0).children)
    .forEach(function(td, index) {
      let thHeader = document.querySelectorAll('thead tr th').item(index);
      thHeader.style.minWidth = `${td.offsetWidth}px`;
      thHeader.style.maxWidth = `${td.offsetWidth}px`;

      let tdFooter = document.querySelectorAll('tfoot tr td').item(index);
      tdFooter.style.minWidth = `${td.offsetWidth}px`;
      tdFooter.style.maxWidth = `${td.offsetWidth}px`;
    });
}

function getInputValue(inputName) {
  return document.querySelector(`input[name='${inputName}']`).value;
}

function setInputValue(inputName, inputValue) {
  document.querySelector(`input[name='${inputName}']`).value = inputValue;
}

function getInputNumberValue(inputName) {
  return parseFloat(getInputValue(inputName)) || 0;
}

function getRequiredInputNumberValue(inputName) {
  let value = getInputNumberValue(inputName);

  if (!value) {
    throwError(`${inputName} is required!`);
  }
  
  return value;
}

function saveRow() {  
  let savedData = getData();
  let winner = getRequiredInputNumberValue('winner_trap');

  let row = {
    race_info: getInputValue('race_info'),
    input: {
      race_distance: getRequiredInputNumberValue('race_distance'),
      odd_trap_1: getInputNumberValue('odd_trap_1'),
      odd_trap_2: getInputNumberValue('odd_trap_2'),
      odd_trap_3: getInputNumberValue('odd_trap_3'),
      odd_trap_4: getInputNumberValue('odd_trap_4'),
      odd_trap_5: getInputNumberValue('odd_trap_5'),
      odd_trap_6: getInputNumberValue('odd_trap_6'),
      odd_trap_7: getInputNumberValue('odd_trap_7'),
      odd_trap_8: getInputNumberValue('odd_trap_8'),
      odd_trap_9: getInputNumberValue('odd_trap_9'),
      odd_trap_10: getInputNumberValue('odd_trap_10'),
    },
    output: {
      winner_trap_1: winner === 1 ? 1 : 0,
      winner_trap_2: winner === 2 ? 1 : 0,
      winner_trap_3: winner === 3 ? 1 : 0,
      winner_trap_4: winner === 4 ? 1 : 0,
      winner_trap_5: winner === 5 ? 1 : 0,
      winner_trap_6: winner === 6 ? 1 : 0,
      winner_trap_7: winner === 7 ? 1 : 0,
      winner_trap_8: winner === 8 ? 1 : 0,
      winner_trap_9: winner === 9 ? 1 : 0,
      winner_trap_10: winner === 10 ? 1 : 0,
    },
  };
  
  if (savedData.length) {
    ++newRows;
    evaluatePrecisions(row.input, winner);
  }

  savedData.push(row);
  setData(savedData);
 
  if (savedData.length === 1) {
    trainModels();
  }

  loadData();
}

function evaluatePrecisions(race, winner) {
  let prediction;

  prediction = leakyReluModel.run(race);
  validatePrediction(prediction);
  if (prediction[`winner_trap_${winner}`] >= 0.25) {
    ++leakyReluModelPrecision;
  }

  prediction = reluModel.run(race);
  validatePrediction(prediction);
  if (prediction[`winner_trap_${winner}`] >= 0.25) {
    ++reluModelPrecision;
  }

  prediction = sigmoidModel.run(race);
  validatePrediction(prediction);
  if (prediction[`winner_trap_${winner}`] >= 0.25) {
    ++sigmoidModelPrecision;
  }

  prediction = tanhModel.run(race);
  validatePrediction(prediction);
  if (prediction[`winner_trap_${winner}`] >= 0.25) {
    ++tanhModelPrecision;
  }

  console.log(`
    leaky-relu: ${(leakyReluModelPrecision/newRows*100).toFixed(2)}%
    relu: ${(reluModelPrecision/newRows*100).toFixed(2)}%
    sigmoid: ${(sigmoidModelPrecision/newRows*100).toFixed(2)}%
    tanh: ${(tanhModelPrecision/newRows*100).toFixed(2)}%
  `);
}

function getData() {
  return JSON.parse(localStorage.data || "[]");
}

function setData(data) {
  localStorage.data = JSON.stringify(data);
}

function loadData() {
  data = getData();
  data = filterData(data);
  
  document.querySelector('tbody').innerHTML = '';
  data.forEach(function(row) {
    let winnerTrap = Object.values(row.output).indexOf(1) + 1;

    document.querySelector('tbody').insertAdjacentHTML(
      'beforeend',
      `
      <tr onclick="editRow('${row.race_info}')" ondblclick="deleteRow('${row.race_info}')">
        <td width="24%">${row.race_info}</td>
        <td width="10%">${row.input.race_distance}</td>
        <td width="6%" class='${winnerTrap === 1 ? 'text-success' : ''}'>${row.input.odd_trap_1.toFixed(2)}</td>
        <td width="6%" class='${winnerTrap === 2 ? 'text-success' : ''}'>${row.input.odd_trap_2.toFixed(2)}</td>
        <td width="6%" class='${winnerTrap === 3 ? 'text-success' : ''}'>${row.input.odd_trap_3.toFixed(2)}</td>
        <td width="6%" class='${winnerTrap === 4 ? 'text-success' : ''}'>${row.input.odd_trap_4.toFixed(2)}</td>
        <td width="6%" class='${winnerTrap === 5 ? 'text-success' : ''}'>${row.input.odd_trap_5.toFixed(2)}</td>
        <td width="6%" class='${winnerTrap === 6 ? 'text-success' : ''}'>${row.input.odd_trap_6.toFixed(2)}</td>
        <td width="6%" class='${winnerTrap === 7 ? 'text-success' : ''}'>${row.input.odd_trap_7.toFixed(2)}</td>
        <td width="6%" class='${winnerTrap === 8 ? 'text-success' : ''}'>${row.input.odd_trap_8.toFixed(2)}</td>
        <td width="6%" class='${winnerTrap === 9 ? 'text-success' : ''}'>${row.input.odd_trap_9.toFixed(2)}</td>
        <td width="6%" class='${winnerTrap === 10 ? 'text-success' : ''}'>${row.input.odd_trap_10.toFixed(2)}</td>
        <td width="6%">${ document.querySelectorAll('thead th img')[ ++winnerTrap ]?.cloneNode().outerHTML }</td>
      </tr>
    `);
  });

  stickyTable();
}

function filterData(data) {
  data = data
    .filter(row => row.race_info.toLowerCase().indexOf(getInputValue('race_info_filter').toLowerCase()) > -1)
    .filter(row => !getInputNumberValue('race_distance_filter') || row.input.race_distance === getInputNumberValue('race_distance_filter'));

  if (document.querySelector(`input[name='ignore_filters']`).checked) {
    return data;
  }

  return data
    .filter(row => document.querySelector(`input[name='odd_trap_1_filter']`).checked ? !!row.input.odd_trap_1 : !row.input.odd_trap_1)
    .filter(row => document.querySelector(`input[name='odd_trap_2_filter']`).checked ? !!row.input.odd_trap_2 : !row.input.odd_trap_2)
    .filter(row => document.querySelector(`input[name='odd_trap_3_filter']`).checked ? !!row.input.odd_trap_3 : !row.input.odd_trap_3)
    .filter(row => document.querySelector(`input[name='odd_trap_4_filter']`).checked ? !!row.input.odd_trap_4 : !row.input.odd_trap_4)
    .filter(row => document.querySelector(`input[name='odd_trap_5_filter']`).checked ? !!row.input.odd_trap_5 : !row.input.odd_trap_5)
    .filter(row => document.querySelector(`input[name='odd_trap_6_filter']`).checked ? !!row.input.odd_trap_6 : !row.input.odd_trap_6)
    .filter(row => document.querySelector(`input[name='odd_trap_7_filter']`).checked ? !!row.input.odd_trap_7 : !row.input.odd_trap_7)
    .filter(row => document.querySelector(`input[name='odd_trap_8_filter']`).checked ? !!row.input.odd_trap_8 : !row.input.odd_trap_8)
    .filter(row => document.querySelector(`input[name='odd_trap_9_filter']`).checked ? !!row.input.odd_trap_9 : !row.input.odd_trap_9)
    .filter(row => document.querySelector(`input[name='odd_trap_10_filter']`).checked ? !!row.input.odd_trap_10 : !row.input.odd_trap_10);
}

function trainModels() {
  let savedData = getData();

  if (!savedData.length) {
    return;
  }

  trainConfig = {
    errorThresh: 0.005,
  };
  
  leakyReluModel.train(savedData, trainConfig);
  reluModel.train(savedData, trainConfig);
  sigmoidModel.train(savedData, trainConfig);
  tanhModel.train(savedData, trainConfig);
}

function getBestModel() {
  let bestPrecision = Math.max(leakyReluModelPrecision, reluModelPrecision, sigmoidModelPrecision, tanhModelPrecision);

  if (leakyReluModelPrecision === bestPrecision) {
    console.log('using leaky-relu...');
    return leakyReluModel;
  }

  if (reluModelPrecision === bestPrecision) {
    console.log('using relu...');
    return reluModel;
  }

  if (sigmoidModelPrecision === bestPrecision) {
    console.log('using sigmoid...');
    return sigmoidModel;
  }

  if (tanhModelPrecision === bestPrecision) {
    console.log('using tanh...');
    return tanhModel;
  }

  throw 'Precision not found.';
}

function predictWinner(useFilters = false) {
  let race = {
    race_distance: getRequiredInputNumberValue('race_distance'),
    odd_trap_1: getInputNumberValue('odd_trap_1'),
    odd_trap_2: getInputNumberValue('odd_trap_2'),
    odd_trap_3: getInputNumberValue('odd_trap_3'),
    odd_trap_4: getInputNumberValue('odd_trap_4'),
    odd_trap_5: getInputNumberValue('odd_trap_5'),
    odd_trap_6: getInputNumberValue('odd_trap_6'),
    odd_trap_7: getInputNumberValue('odd_trap_7'),
    odd_trap_8: getInputNumberValue('odd_trap_8'),
    odd_trap_9: getInputNumberValue('odd_trap_9'),
    odd_trap_10: getInputNumberValue('odd_trap_10'),
  };

  let prediction = getBestModel().run(race);
  validatePrediction(prediction);

  alert(`
    01\t${getIcon(prediction.winner_trap_1, race.odd_trap_1)}\t${prediction.winner_trap_1?.toFixed(3)}\t${race.odd_trap_1.toFixed(2)}\n
    02\t${getIcon(prediction.winner_trap_2, race.odd_trap_2)}\t${prediction.winner_trap_2?.toFixed(3)}\t${race.odd_trap_2.toFixed(2)}\n
    03\t${getIcon(prediction.winner_trap_3, race.odd_trap_3)}\t${prediction.winner_trap_3?.toFixed(3)}\t${race.odd_trap_3.toFixed(2)}\n
    04\t${getIcon(prediction.winner_trap_4, race.odd_trap_4)}\t${prediction.winner_trap_4?.toFixed(3)}\t${race.odd_trap_4.toFixed(2)}\n
    05\t${getIcon(prediction.winner_trap_5, race.odd_trap_5)}\t${prediction.winner_trap_5?.toFixed(3)}\t${race.odd_trap_5.toFixed(2)}\n
    06\t${getIcon(prediction.winner_trap_6, race.odd_trap_6)}\t${prediction.winner_trap_6?.toFixed(3)}\t${race.odd_trap_6.toFixed(2)}\n
    07\t${getIcon(prediction.winner_trap_7, race.odd_trap_7)}\t${prediction.winner_trap_7?.toFixed(3)}\t${race.odd_trap_7.toFixed(2)}\n
    08\t${getIcon(prediction.winner_trap_8, race.odd_trap_8)}\t${prediction.winner_trap_8?.toFixed(3)}\t${race.odd_trap_8.toFixed(2)}\n
    09\t${getIcon(prediction.winner_trap_9, race.odd_trap_9)}\t${prediction.winner_trap_9?.toFixed(3)}\t${race.odd_trap_9.toFixed(2)}\n
    10\t${getIcon(prediction.winner_trap_10, race.odd_trap_10)}\t${prediction.winner_trap_10?.toFixed(3)}\t${race.odd_trap_10.toFixed(2)}
  `);
}

function getIcon(probability, odd) {
  if (odd === 0) {
    return "❌\tOUT";
  }

  if (probability >= 0.25) {
    return "📌\tBUY";
  }

  if (probability <= 0.05 && odd <= 12) {
    return "✅\tLAY";
  }

  return "⚠️\tOUT";
}

function autoFill() {
  getInputValue('auto_fill')
    .trim()
    .split(";")
    .forEach(function(value, index) {
      document.querySelectorAll('tfoot input').item(index).value = value;
    });

  setInputValue('auto_fill', '');
}

function validatePrediction(prediction) {
  console.log('validating prediction...', prediction);

  let hasInvalidPrediction = Object.values(prediction)
    .reduce(function(previous, current) {
      return previous || isNaN(current);
    }, false);

  if (hasInvalidPrediction) {
    throwError('invalid prediction detected!');
  }
}

function throwError(error) {
  alert(error);
  throw error;
}

function viewResult() {
  document.getElementById('view_result').innerHTML = brain.utilities.toSVG(getBestModel(), {
    width: 1280,
    height: 720,
  });
}

function editRow() {
  // TODO
}

function deleteRow(raceInfo) {
  if (!confirm('Do you want to delete this row?')) {
    return;
  }
  
  let savedData = getData();
  savedData = savedData.filter(function(row) {
    return row.race_info !== raceInfo;
  })
  setData(savedData);
  
  loadData();
}
